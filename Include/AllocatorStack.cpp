
#include "AllocatorStack.h"


AllocatorStack::AllocatorStack()
{
    m_pMarker = m_pMemoryBlock;
    m_pBound = static_cast<void *>(reinterpret_cast<char*>(m_pMarker) + MemoryByteSize);

    std::cout << " memory block starts at : " << m_pMemoryBlock << std::endl;
}

AllocatorStack::~AllocatorStack()
{
}

void* AllocatorStack::Allocate(size_t memorybytes, size_t memoryalignment /*= MemoryAlignment*/)
{
    if (memoryalignment % 4 != 0)
    {
        DeleteMemoryBlock();
        assert(false && " memoryalignment must be multiple of 4 ");
    }

    char *temp = reinterpret_cast<char*>(m_pMarker) + memorybytes;
    if (reinterpret_cast<__int64>(temp) > reinterpret_cast<__int64>(m_pBound))
    {
        DeleteMemoryBlock();
        assert(false && " stack memory over ");
    }

    // ex 4: padding = ( 4 -( byte % 4) ) % 4
    size_t padding = (memoryalignment - (memorybytes % memoryalignment))% memoryalignment;

    std::cout << "(+) marker address : " << m_pMarker << "->";

    void *pMarker = m_pMarker;
    m_pMarker = static_cast<void *>(reinterpret_cast<char*>(m_pMarker) + memorybytes + padding);

    std::cout << m_pMarker ;
    std::cout << " ::current marker : " << m_pMarker << std::endl;

    return pMarker;
}

void AllocatorStack::Deallocate(Marker pMarker)
{
    if (IsInvalidMarker(pMarker))
    {
        DeleteMemoryBlock();
        assert(false && " Try to deallocate maker which has been already deallocated ");
    }

    std::cout << "(-) marker address : " << m_pMarker << "->";
    m_pMarker = pMarker;
    std::cout << m_pMarker ;
    std::cout << " ::current marker : " << m_pMarker << std::endl;
}

void* AllocatorStack::GetMarker()
{
    return m_pMarker;
}

void AllocatorStack::Clear()
{
    m_pMarker = m_pMemoryBlock;
}

bool AllocatorStack::IsInvalidMarker(Marker pMarker)
{
    if (pMarker >= m_pMarker)
    {
        return true;
    }

    if (pMarker == nullptr)
    {
        return true;
    }

    return false;
}

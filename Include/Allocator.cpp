
#include "Allocator.h"


Allocator::Allocator()
{
    m_pMemoryBlock = operator new(MemoryByteSize);
}

Allocator::~Allocator()
{
    DeleteMemoryBlock();
}

void Allocator::DeleteMemoryBlock()
{
    if (m_pMemoryBlock)
    {
        operator delete(m_pMemoryBlock);
    }
}

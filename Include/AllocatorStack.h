#pragma once

#include "Allocator.h"



class AllocatorStack: public Allocator
{
    template <typename T>
    friend class TStackAllocator;

    using Marker = void *;

private:
    void* m_pBound = nullptr;
    Marker m_pMarker = nullptr;

public:
    AllocatorStack();
    ~AllocatorStack();

    void* Allocate(size_t memorybytes, size_t memoryalignment = MemoryAlignment) override;
    void Deallocate(Marker pMarker);

    Marker GetMarker();

    void Clear();

private:
    bool IsInvalidMarker(Marker pMarker);
};

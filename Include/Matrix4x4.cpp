
#include "Matrix4x4.h"
#include "Vector3.h"


Matrix4x4 Matrix4x4::Identity4x4 = Matrix4x4(Vector4(1, 0, 0, 0), Vector4(0, 1, 0, 0), Vector4(0, 0, 1, 0), Vector4(0, 0, 0, 1));

Matrix4x4::Matrix4x4()
{
    row[0] = Vector4::Zero;
    row[1] = Vector4::Zero;
    row[2] = Vector4::Zero;
    row[3] = Vector4::Zero;
}

Matrix4x4::Matrix4x4(const Matrix4x4 & m)
{
    row[0] = m.row[0];
    row[1] = m.row[1];
    row[2] = m.row[2];
    row[3] = m.row[3];
}

Matrix4x4::Matrix4x4(const Vector4 & m0, const Vector4 & m1, const Vector4 & m2, const Vector4 & m3)
{
    row[0] = m0;
    row[1] = m1;
    row[2] = m2;
    row[3] = m3;
}

Matrix4x4::Matrix4x4(const Vector4 m[4])
{
    row[0] = m[0];
    row[1] = m[1];
    row[2] = m[2];
    row[3] = m[3];
}

Matrix4x4 Matrix4x4::operator=(const Matrix4x4 & m)
{
    row[0] = m.row[0];
    row[1] = m.row[1];
    row[2] = m.row[2];
    row[3] = m.row[3];

    return *this;
}

Matrix4x4 Matrix4x4::operator*(const Matrix4x4 & m) const
{
    Matrix4x4 matrix;

    matrix._00 = _00 * m._00 + _01 * m._10 + _02 * m._20 + _03 * m._30;
    matrix._01 = _00 * m._01 + _01 * m._11 + _02 * m._21 + _03 * m._31;
    matrix._02 = _00 * m._02 + _01 * m._12 + _02 * m._22 + _03 * m._32;
    matrix._03 = _00 * m._03 + _01 * m._13 + _02 * m._23 + _03 * m._33;

    matrix._10 = _10 * m._00 + _11 * m._10 + _12 * m._20 + _13 * m._30;
    matrix._11 = _10 * m._01 + _11 * m._11 + _12 * m._21 + _13 * m._31;
    matrix._12 = _10 * m._02 + _11 * m._12 + _12 * m._22 + _13 * m._32;
    matrix._13 = _10 * m._03 + _11 * m._13 + _12 * m._23 + _13 * m._33;

    matrix._20 = _20 * m._00 + _21 * m._10 + _22 * m._20 + _23 * m._30;
    matrix._21 = _20 * m._01 + _21 * m._11 + _22 * m._21 + _23 * m._31;
    matrix._22 = _20 * m._02 + _21 * m._12 + _22 * m._22 + _23 * m._32;
    matrix._23 = _20 * m._03 + _21 * m._13 + _22 * m._23 + _23 * m._33;    

    matrix._30 = _30 * m._00 + _31 * m._10 + _32 * m._20 + _33 * m._30;
    matrix._31 = _30 * m._01 + _31 * m._11 + _32 * m._21 + _33 * m._31;
    matrix._32 = _30 * m._02 + _31 * m._12 + _32 * m._22 + _33 * m._32;
    matrix._33 = _30 * m._03 + _31 * m._13 + _32 * m._23 + _33 * m._33;

    return matrix;
}

Vector4 Matrix4x4::operator*(const Vector4 & v) const
{
    // 책에 있는 레지스터 연산 적용해봐야함
    Vector4 vResult;

    vResult[0] = _00 * v[0] + _01 * v[1] + _02 * v[2] + _03 * v[3];
    vResult[1] = _10 * v[0] + _11 * v[1] + _12 * v[2] + _13 * v[3];
    vResult[2] = _20 * v[0] + _21 * v[1] + _22 * v[2] + _23 * v[3];
    vResult[3] = _30 * v[0] + _31 * v[1] + _32 * v[2] + _33 * v[3];

    return vResult;
}

void Matrix4x4::operator*=(const Matrix4x4 & m)
{
    *this = (*this)*m;
}

Vector4& Matrix4x4::operator[](size_t index)
{
    assert(index >= 0 && index < 4);
    return row[index];
}

const Vector4& Matrix4x4::operator[](size_t index) const
{
    assert(index >= 0 && index < 4);
    return row[index];
}

Matrix4x4 Matrix4x4::Transpose()
{
    return Matrix4x4();
}

Matrix4x4 Matrix4x4::Inverse()
{
    return Matrix4x4();
}

Matrix4x4 Matrix4x4::Rotation(float x, float y, float z)
{
    Matrix4x4 matRotX, matRotY, matRotZ;
    matRotX = (*this)*RotationX(x);
    matRotY = matRotX*RotationY(y);
    matRotZ = matRotY*RotationZ(z);
    return matRotZ;
}

Matrix4x4 Matrix4x4::RotationX(float x)
{
    Matrix4x4 rotX;

    float rad = math::DegreeToRadian(x);

    rotX[0] = Vector4(1, 0, 0, 0);
    rotX[1] = Vector4(0, cosf(rad), -sinf(rad), 0);
    rotX[2] = Vector4(0, sinf(rad), cosf(rad), 0);
    rotX[2] = Vector4(0, 0, 0, 1);

    return rotX;
}

Matrix4x4 Matrix4x4::RotationY(float y)
{
    Matrix4x4 rotY;

    float rad = math::DegreeToRadian(y);

    rotY[0] = Vector4(cosf(rad), 0, sinf(rad), 0);
    rotY[1] = Vector4(0, 1, 0, 0);
    rotY[2] = Vector4(-sinf(rad), 0, cosf(rad), 0);
    rotY[2] = Vector4(0, 0, 0, 1);

    return rotY;
}

Matrix4x4 Matrix4x4::RotationZ(float z)
{
    Matrix4x4 rotZ;

    float rad = math::DegreeToRadian(z);

    rotZ[0] = Vector4(cosf(rad), -sinf(rad), 0, 0);
    rotZ[1] = Vector4(sinf(rad), cosf(rad), 0, 0);
    rotZ[2] = Vector4(0, 0, 1, 0);
    rotZ[2] = Vector4(0, 0, 0, 1);

    return rotZ;
}

Matrix4x4 Matrix4x4::RotationAxis(float fAngle, Vector3 vAxis)
{
    return Matrix4x4();
}

float Matrix4x4::determinant4x4() const
{
    return 0.0f;
}

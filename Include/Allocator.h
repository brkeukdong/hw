#pragma once

#include <iostream>
#include <limits>
#include <assert.h>

class Allocator
{
protected:
    Allocator();
    virtual ~Allocator();

protected:
    static constexpr size_t MemoryByteSize = 1024 * 1024;
    static constexpr size_t MemoryAlignment = 4;
    void* m_pMemoryBlock = nullptr;

public:
    virtual void* Allocate(size_t memorybytes, size_t memoryalignment = MemoryAlignment) = 0;

protected:
    void DeleteMemoryBlock();
};


#pragma once

#include "AllocatorStack.h"

/*
not yet complete
*/
template <typename T>
class TStackAllocator
{
public:
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;
    typedef T value_type;

    template <typename U>
    struct rebind
    {
        using other = TStackAllocator<U>;
    };

    template <typename U>
    friend class TStackAllocator;

public:
    TStackAllocator() = default;
    ~TStackAllocator() = default;

    template <typename U>
    TStackAllocator(const TStackAllocator<U> &other)
    {
    };

    static pointer allocate(size_type n)
    {
        std::cout << "allocate" << std::endl;
        return static_cast<pointer>(memorystack.allocate(sizeof(T) * n));
    }

   static void deallocate(pointer p, size_type n)
    {
       std::cout << "deallocate" << std::endl;
       memorystack.deallocate(sizeof(T)*n);
    }
private:
    static AllocatorStack memorystack;
};

template <typename T> AllocatorStack TStackAllocator<T>::memorystack;
